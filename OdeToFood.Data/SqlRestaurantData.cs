﻿using OdeToFood.Core;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace OdeToFood.Data
{
    public class SqlRestaurantData : IRestaurantData
    {
        private readonly OdeToFoodDbContext _dbContext;

        public SqlRestaurantData(OdeToFoodDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Restaurant Add(Restaurant restaurant)
        {
            _dbContext.Restaurants.Add(restaurant);
            return restaurant;
        }

        public int Commit() => _dbContext.SaveChanges();

        public Restaurant Delete(int id)
        {
            var restaurant = GetById(id);
            if (restaurant != null)
                _dbContext.Restaurants.Remove(restaurant);

            return restaurant;
        }

        public Restaurant GetById(int id) => _dbContext.Restaurants.FirstOrDefault(r => r.Id == id);

        public int GetCountOfRestaurants() => _dbContext.Restaurants.Count();

        public IEnumerable<Restaurant> GetRestaurantsByName(string name)
        {
            return _dbContext.Restaurants
                            .Where(r => r.Name.StartsWith(name) || string.IsNullOrEmpty(name))
                            .OrderBy(r => r.Name).ToList();
        }

        public Restaurant Update(Restaurant updateRestaurant)
        {
            var restaurant = _dbContext.Restaurants.Attach(updateRestaurant);
            restaurant.State = EntityState.Modified;
            
            return updateRestaurant;
        }
    }
}
