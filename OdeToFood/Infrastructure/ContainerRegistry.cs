﻿using OdeToFood.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StructureMap;

namespace OdeToFood.Infrastructure
{
    public class ContainerRegistry : Registry
    {        
        public ContainerRegistry()
        {
            For<IRestaurantData>().Use<SqlRestaurantData>().ContainerScoped();
        }
    }
}
