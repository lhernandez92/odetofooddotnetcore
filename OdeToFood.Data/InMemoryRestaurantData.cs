﻿using OdeToFood.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OdeToFood.Data
{
    public class InMemoryRestaurantData : IRestaurantData
    {
        private readonly List<Restaurant> Restaurants;

        public InMemoryRestaurantData()
        {
            Restaurants = new List<Restaurant>()
                {
                    new Restaurant{ Id = 1, Name = "Scott's Pizza", Location = "Maryland" , Cuisine = CuisineType.Italian},
                    new Restaurant{ Id = 2, Name = "Cinnamon Club", Location = "London" , Cuisine = CuisineType.Indian},
                    new Restaurant{ Id = 3, Name = "La Costa", Location = "California" , Cuisine = CuisineType.Mexican}
                };
        }

        public Restaurant Add(Restaurant restaurant)
        {
            Restaurants.Add(restaurant);
            restaurant.Id = Restaurants.Max(r => r.Id) + 1;
            return restaurant;
        }

        public int Commit() => 0;

        public Restaurant Delete(int id)
        {
            var restaurant = Restaurants.FirstOrDefault(r => r.Id == id);
            if (restaurant != null)
                Restaurants.Remove(restaurant);

            return restaurant;
        }

        public Restaurant GetById(int id) => Restaurants.SingleOrDefault(x => x.Id == id);

        public int GetCountOfRestaurants() => Restaurants.Count;

        public IEnumerable<Restaurant> GetRestaurantsByName(string name = null)
        {
            return from result in Restaurants
                   where string.IsNullOrEmpty(name) || result.Name.StartsWith(name)
                   orderby result.Name 
                   select result;
        }

        public Restaurant Update(Restaurant updateRestaurant)
        {
            var restaurant = Restaurants.SingleOrDefault(r => r.Id == updateRestaurant.Id);
            if(restaurant != null)
            {
                restaurant.Name = updateRestaurant.Name;
                restaurant.Location = updateRestaurant.Location;
                restaurant.Cuisine = updateRestaurant.Cuisine;
            }

            return restaurant;
        }
    }
}
